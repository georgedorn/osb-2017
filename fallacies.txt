@terriko
yourlogicalfallacyis.com

fallacies often make the orator seem clever, though the logic is unsound
but fallacies are still-working shortcuts that work still
- because brains need shortcuts, critical thinking takes work

science communication requires time-limited public speaking
- e.g. presenting a lot of work in a conference setting
- or discussing on twitter or media interview sound bites
- or grant proposals

fallacies to help choose what parts to shortcut or gloss over

see: how biology explains difference in math ability (it doesn't) "presentation"
fallacies in that presentation:
- False Authority - claiming mathematics ability, but mathematicians don't study populations this way
-- sociologists and psychologists study this
-- Use False Authority to shortcut providing credentials
-- often there's no such thing as an authority on a subject (esp during grant proposals)

- Strawman (show the wrong argument in overly simplified terms)
-- Shortcut, makes argument easier
-- provides a toehold
-- can use this positively if you identify the strawman
-- can also refine it

- Anecdotal evidence
-- single view of world
-- good when there's no better data
-- you're lazy
-- humans are more susceptible to stories, remember them better
-- can mitigate this by treating as hypothesis, being honest about nature, link to real data

- Fallacy of single cause
-- just because math ability doesn't account for CS differences doesn't mean there's no biological cause
- +False Dichotomy
-- do these to avoid muddying waters
-- don't know all other possibilities
-- skip addressing minor points where you have more data covering those arguments
-- mitigate by being honest, treating the summary as a sketch of the full argument

More:

Appeal to probability
- something is likely, therefore it is true
-- infer something is true because it probably is
-- see webdevs setting security policies
--- which google later proved
-- use when you can't quite get the data you need, ancillary hypothesis you hope somebody else proves

Nirvana fallacy:
- reject imperfect solutions
-- usually there's no such thing as a perfect solution
- but might want to use it for social issues, especially if the imperfect solution prevents a better one from being implemented

Argument from fallacy:
- if an argument uses fallacies, the conclusion is false
-- fallacious arguments can be true


Dangerous ones to avoid:
- Ad hominem attacks
- cherry picking
-- confirmation bias - pick data that supports our position
- no true scotsman - refine criteria for data
-- also moving goalposts

Ethics:
A Presentation is more like marketing than a presentation of science; the paper is the real science.
Fallacies can be used ethically (see mitigation strategies, be honest about hand-waving or other uses of fallacies)


Books:
Informal Logic
Thinking Fast and Slow
blink (Malcom Gladwell)
Age of Persuasion (Terry O'Reilly)

Podcast: Under The Influence (particularly DEFCON 19: The Art of Trolling)
